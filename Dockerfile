FROM python:3.6.0-alpine

ADD index.py /
ADD proxy.sh /

ENV http_proxy 'http://10.11.25.2:3128'
ENV https_proxy 'http://10.11.25.2:3128'

RUN ["chmod", "+x", "/proxy.sh"]
RUN /proxy.sh

# System packages
RUN apk update && apk add build-base jpeg-dev zlib-dev
ENV LIBRARY_PATH=/lib:/usr/lib

RUN pip install pystrich

CMD [ "python", "./index.py" ]