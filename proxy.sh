#!/usr/bin/env sh

if ping -c 1 ix.de ; then
    echo "direct internet. doing nothing!"
else
    echo "proxy environment detected. setting proxy!"
    #export http_proxy=http://10.11.25.2:3128
    #export https_proxy=http://10.11.25.2:3128
fi